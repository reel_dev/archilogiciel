const fastify = require('fastify')({ logger: true });
const axios = require('axios');

fastify.register(require('@fastify/cors'), {
    hook: 'preHandler',
    delegator: (req, callback) => {
        const corsOptions = {
            // This is NOT recommended for production as it enables reflection exploits
            origin: true
        };

        // do not include CORS headers for requests from localhost
        if (/^localhost$/m.test(req.headers.origin)) {
            corsOptions.origin = false
        }

        // callback expects two parameters: error and options
        callback(null, corsOptions)
    },
})

fastify.register(async function (fastify) {
    fastify.get('/', (req, reply) => {
        reply.send({ hello: 'world' })
    })
})

const verifyToken = async (token) => {
    try {
        const options = {
            method: 'GET',
            url: 'http://localhost:8080/realms/master/protocol/openid-connect/userinfo',
            headers: {
                Authorization: `Bearer ${token}`,
            },
        };

        const response = await axios(options);
        return response.data;
    } catch (error) {
        throw new Error('Token invalide');
    }
};

fastify.get('/clients', async (req, res) => {
    try {
        const authorizationHeader = req.headers.authorization;
        if (!authorizationHeader || !authorizationHeader.startsWith('Bearer ')) {
           res.status(401)
        }

        const token = authorizationHeader.substring('Bearer '.length);

        // Vérifier le token Keycloak
        const userInfo = await verifyToken(token);

        res.status(200).send(userInfo);
    } catch (error) {
        console.error('Erreur de validation du token :', error.message);
        res.status(401).send({ message: 'Token invalide' });
    }
});

fastify.setNotFoundHandler((request, reply) => {
    reply.code(404).send({ message: 'Route non trouvée' });
});

const start = async () => {
    try {
        await fastify.listen(3000);
        fastify.log.info(`Serveur Fastify démarré sur ${fastify.server.address().port}`);
    } catch (err) {
        fastify.log.error(err);
        process.exit(1);
    }
};
start();
